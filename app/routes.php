<?php
/**
 * Created by IntelliJ IDEA.
 * User: w10
 * Date: 4/30/2016
 * Time: 1:42 AM
 */

$sUser = $app['session']->get('Username');

if ($app['devmode'] == true && $sUser == "") {

    $app->get("/sess_check", function () use ($app) {
        if (null === $user = $app['session']->get('Username')) {
            return "false";
        } else {
            return "true";
        }
    })
        ->bind('sess_check')
    ;

    require_once 'routes/login.php';

    $app->get('{url}', function($url, \Symfony\Component\HttpFoundation\Request $req) use($app) {
        return $app['twig']->render('devmode.twig', array(
            'title' => 'Developement Mode'
        ));
    })->assert('url', '.+');

    $app->get('/', function () use ($app) {
        return $app['twig']->render('devmode.twig', array(
            'title' => 'Development Mode'
        ));
    });
} else {
    require_once 'routes/home.php';
    require_once 'routes/bot.php';
    require_once 'routes/settings.php';
    require_once 'routes/login.php';
    require_once 'routes/Twitter/tweets.php';
}