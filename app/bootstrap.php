<?php
/**
 * Created by IntelliJ IDEA.
 * User: w10
 * Date: 5/3/2016
 * Time: 2:26 PM
 */
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__. '/routes/data.php';

$app = new Silex\Application();

$app->register(new \Silex\Provider\SessionServiceProvider(), array(
    'session.storage.save_path' => '/var/sites/tmp/sessions/thamian'
));

$app->register(new \Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../app/views',
));

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

use Silex\Application;

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_mysql',
        'host' => 'localhost',
        'dbname' => db_name,
        'user' => db_user,
        'password' => db_passwd,
        'port' => 3306
    ),
));

$app['twig'] = $app->share($app->extend('twig', function ($twig, $app) {
    $twig->addGlobal('gTitle', 'ThamianPlay');

    if (null !== $sUser = $app['session']->get('Username')) {
        $twig->addGlobal('Username', $app['session']->get('Username'));
    }

    if (null !== $sGroup = $app['session']->get('Group')) {
        $twig->addGlobal('Group', $app['session']->get('Group'));
    }

    $sel_sup = "SELECT COUNT(*) AS Anzahl FROM Contact WHERE Status = 'Offen'";
    $sup = $app['db']->fetchAssoc($sel_sup);

    $sel_gb = "SELECT COUNT(*) AS Anzahl FROM Guestbook WHERE Verified = '0'";
    $gb_ct = $app['db']->fetchAssoc($sel_gb);

    if (isset($sup)) {
        $twig->addGlobal('SupCount', $sup);
    }

    if (isset($gb_ct)) {
        $twig->addGlobal('GB_Count', $gb_ct);
    }

    return $twig;
}));

$app['twig']->addExtension(new Twig_Extensions_Extension_Text());

$app->register(new Silex\Provider\ValidatorServiceProvider());

$app['debug'] = true;
$app['devmode'] = true;
$app['secret_salt'] = st_secret_salt;
$app['pdo'] = new PDO('mysql:dbname=' . db_name . ';host=localhost', db_user, db_passwd);

require __DIR__ . '/../app/routes.php';

return $app;