<?php
/**
 * Created by IntelliJ IDEA.
 * User: w10
 * Date: 5/2/2016
 * Time: 5:45 PM
 */
$app->get('/bot/quotes', function () use ($app) {

    return $app['twig']->render('home.twig', array(
        'title' => 'Quotes'
    ));
})->bind('bot.quotes');

$app->get('/bot/quotes/json', function () use ($app) {
    require 'data.php';
    try
    {
        $client = new GuzzleHttp\Client([
            'headers' => [
                'webauth' => webauth
            ]
        ]);
        $response = $client->get('http://' . host . ':' . port . '/dbquery?table=quotes&getKeys');
        $result = $response->getBody();
        $object = json_decode($result);

        $quotes = array();
        foreach($object->table->keylist as $key=>$keys)
        {
            $response = $client->get('http://' . host . ':' . port . '/dbquery?table=quotes&getData=' . $key);
            $result = $response->getBody();
            $object = json_decode($result);
            $timestamp = (json_decode($object->table->value)[2]) / 1000;
            $datum = date('d.m.Y, H:i:s', $timestamp);
            $src = json_decode($object->table->value);
            $quotes[$key] = array_replace($src, array(2 => $datum));
        }
    }
    catch (ClientException $e)
    {
        $response = $e->getResponse();
        $result = $response->getBody();
        $object = json_decode($result);
        echo "Exception has occurred: ". $object->{'error'};
    }

    $elemente = count($quotes);

    return json_encode($quotes, JSON_FORCE_OBJECT);
});

$app->get('/bot/quotes/count', function () use ($app) {
    require 'data.php';
    try
    {
        $client = new GuzzleHttp\Client([
            'headers' => [
                'webauth' => webauth
            ]
        ]);
        $response = $client->get('http://' . host . ':' . port . '/dbquery?table=quotes&getKeys');
        $result = $response->getBody();
        $object = json_decode($result);

        $quotes = array();
        foreach($object->table->keylist as $key=>$keys)
        {
            $response = $client->get('http://' . host . ':' . port . '/dbquery?table=quotes&getData=' . $key);
            $result = $response->getBody();
            $object = json_decode($result);
            $timestamp = (json_decode($object->table->value)[2]) / 1000;
            $datum = date('d.m.Y, H:i:s', $timestamp);
            $src = json_decode($object->table->value);
            $quotes[$key] = array_replace($src, array(2 => $datum));
        }
    }
    catch (ClientException $e)
    {
        $response = $e->getResponse();
        $result = $response->getBody();
        $object = json_decode($result);
        echo "Exception has occurred: ". $object->{'error'};
    }

    $elemente = count($quotes);

    return $elemente;
});

$app->get('/bot/commands', function () use ($app) {
    require 'botConnect.php';

    $commands = $connector->getTable('command')[0];
    ksort($commands);

    return $app['twig']->render('home.twig', array(
        'title' => 'Befehle',
        'commands' => $commands
    ));
})->bind('bot.points');