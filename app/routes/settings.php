<?php
/**
 * Created by IntelliJ IDEA.
 * User: w10
 * Date: 5/2/2016
 * Time: 6:22 PM
 */

$logincheck = function (\Symfony\Component\HttpFoundation\Request $request, \Silex\Application $app) {
    if (null === $user = $app['session']->get('Username')) {
        $app['session']->getFlashBag()->add("Error", "Du musst angemeldet sein, um diese Seite nutzen zu können!");
        return $app->redirect($app['url_generator']->generate('devlogin'));
    }
};

$admincheck = function (\Symfony\Component\HttpFoundation\Request $request, \Silex\Application $app) {
    if (null !== $group = $app['session']->get('Group')) {
        if ($group != "Administrator") {
            $app['session']->clear();
            $app['session']->getFlashBag()->add("Error", "Du musst Administrator sein, um diese Seite nutzen zu können!");
            return $app->redirect($app['url_generator']->generate('devlogin'));
        }
    }
};

$block_check = function (\Symfony\Component\HttpFoundation\Request $request, \Silex\Application $app) {
    $email = $request->get('email');

    if (isset($email) && $email != "") {
        $sel = "SELECT * FROM Block WHERE Email = ?";
        $get_c = $app['db']->fetchAll($sel, array((string) $email));
        $get_em = $app['db']->fetchAssoc($sel, array((string) $email));

        if (count($get_c) > 0) {
            $reason = $get_em['Reason'];
            $perma = $get_em['permanent'];

            if ($perma == true) {
                $app['session']->getFlashBag()->add('Error', 'Deine E-Mail Adresse wurde permanent aus dem Grund <b>' . $reason . '</b> gesperrt!');
                return $app->redirect($app['url_generator']->generate('home'));
            } else {
                $date_until = $get_em['blocked_until'];
                $app['session']->getFlashBag()->add('Error', 'Deine E-Mail Adresse wurde bis zum ' . $date_until . ' aus dem Grund <b>' . $reason . '</b> gesperrt!');
                return $app->redirect($app['url_generator']->generate('home'));
            }
        }
    }
};

$app->get('settings/user/genpw', function () use ($app) {
    function generateRandomString($length = 12) {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }
    return generateRandomString();
})
    ->before($logincheck)
    ->before($admincheck)
;

$app->get('/settings/user', function () use ($app) {
    $sql_gu = "SELECT u.`ID`, u.`Username`, u.`Password`, u.Email, u.Last_Login, g.`Name` FROM `User` u JOIN `Groups` g ON u.gID = g.ID";
    $sql_gg = "SELECT `ID`, `Name` FROM `Groups`";

    $get_user = $app['db']->fetchAll($sql_gu);
    $get_group = $app['db']->fetchAll($sql_gg);

    $app['session']->set('_token', bin2hex(random_bytes(32)));

    return $app['twig']->render('backend.twig', array(
        'title' => 'Benutzerverwaltung',
        'users' => $get_user,
        'groups' => $get_group
    ));
})
    ->bind('settings.user')
    ->before($logincheck)
    ->before($admincheck)
;

$app->post('/settings/user/add', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    require_once 'data.php';

    $username = $request->get('username');
    $password = hash('whirlpool', $request->get('password') . $username . $app['secret_salt']);
    $password_rp = hash('whirlpool', $request->get('password_rp') . $username . $app['secret_salt']);
    $group = $request->get('group');
    $email = $request->get('email');

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    $errors = $app['validator']->validateValue($email, new \Symfony\Component\Validator\Constraints\Email());

    if (isset($ptoken) && isset($stoken) && $ptoken == $stoken) {
        if ($password == $password_rp) {
            if (count($errors) > 0) {
                $app['session']->getFlashBag()->add('Error', 'Die E-Mail Adresse ist nicht gültig!');
                return $app->redirect($app['url_generator']->generate('settings.user'));
            } else {
                $sel_user = "SELECT COUNT(Username) AS rUsers FROM `User` WHERE `Username` = ?";
                $sel_email = "SELECT COUNT(Email) AS rEmail FROM `User` WHERE `Email` = ?";

                $get_user = $app['db']->fetchAssoc($sel_user, array((string) $username));
                $get_email = $app['db']->fetchAssoc($sel_email, array((string) $email));

                $mail_body = '
                    <html>
                        <p>Deine Login-Daten für <a href="https://www.thamian.de" target="_blank">Thamian.de</a></p>
                        <ul>
                            <li><b>Benutzername: </b> ' . $username . '</li>
                            <li><b>Passwort: </b> ' . $request->get("password") . ' (Passwort bitte nach der Anmeldung umgehend <a href="https://gogs.clocxsrv.net/fthielen/thamian/wiki/Passwort-%C3%A4ndern">&auml;ndern</a>.)</li>
                        </ul>
                    </html>
                ';

                if ($get_user["rUsers"] == 0) {
                    if ($get_email["rEmail"] == 0) {
                        $app['db']->insert('User', array(
                            'Username' => $username,
                            'Password' => $password,
                            'gID' => $group,
                            'Email' => $email
                        ));

                        $mail = new PHPMailer();

                        $mail->CharSet = "UTF-8";

                        $mail->isSMTP();
                        $mail->Host = mail_host;
                        $mail->SMTPAuth = true;
                        $mail->Username = mail_username;
                        $mail->Password = mail_pass;
                        $mail->SMTPSecure = "tls";
                        $mail->Port = 587;

                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true
                            )
                        );

                        $mail->setFrom('info@thamian.de', 'Thamian.de');
                        $mail->addAddress($email);
                        $mail->isHTML(true);

                        $mail->Subject = "Deine Login-Daten für Thamian.de";
                        $mail->Body = $mail_body;

                        if (!$mail->send()) {
                            return "Fehler beim senden der Mail! " . $mail->ErrorInfo . "<br />";
                            exit();
                        }

                        $app['session']->getFlashBag()->add('Success', 'Der Benutzer <b>' . $username . '</b> wurde erfolgreich hinzugefügt!');
                        return $app->redirect($app['url_generator']->generate('settings.user'));
                    } else {
                        $app['session']->getFlashBag()->add('Error', 'Ein Benutzer mit dieser E-Mail Adresse existiert bereits!');
                        return $app->redirect($app['url_generator']->generate('settings.user'));
                    }
                } else {
                    $app['session']->getFlashBag()->add('Error', 'Ein Benutzer mit diesem Benutzernamen existiert bereits!');
                    return $app->redirect($app['url_generator']->generate('settings.user'));
                }
            }
        } else {
            $app['session']->getFlashBag()->add('Error', 'Die Passwörter stimmen nicht überein!');
            return $app->redirect($app['url_generator']->generate('settings.user'));
        }
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('settings.user'));
    }
})
    ->bind('settings.user.add')
    ->before($logincheck)
    ->before($admincheck)
;

$app->post('/settings/user/delete', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $uID = $request->get('uID');

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (isset($ptoken) && isset($stoken) && $ptoken == $stoken) {
        $sql_deluser = "SELECT g.Name FROM `User` u JOIN `Groups` g ON u.gID = g.ID WHERE u.ID = ?";
        $get_deluser = $app['db']->fetchAssoc($sql_deluser, array((int) $uID));

        if ($get_deluser['Name'] != "Administrator") {
            $app['db']->delete('User', array(
                'ID' => $uID
            ));

            $app['session']->getFlashBag()->add('Success', 'Der Benutzer wurde erfolgreich gelöscht!');
            return $app->redirect($app['url_generator']->generate('settings.user'));
        } else {
            $app['session']->getFlashBag()->add('Error', 'Administratoren können nicht gelöscht werden!');
            return $app->redirect($app['url_generator']->generate('settings.user'));
        }
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('settings.user'));
    }
})
    ->bind('settings.user.del')
    ->before($logincheck)
    ->before($admincheck)
;

$app->post('/settings/user/chug', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $uID = $request->get('uID');
    $group = $request->get('group');
    $username = $request->get('username');

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (isset($ptoken) && isset($stoken) && $ptoken == $stoken) {
        $sql_chug = "UPDATE `User` SET `gID`= ? WHERE `ID` = ?";
        $app['db']->executeUpdate($sql_chug, array((int) $group, (int) $uID));

        $app['session']->getFlashBag()->add('Success', 'Die Gruppe des Benutzers ' . $username . ' wurde erfolgreich geändert!');
        return $app->redirect($app['url_generator']->generate('settings.user'));
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('settings.user'));
    }
})
    ->bind('settings.user.chug')
    ->before($logincheck)
    ->before($admincheck)
;

$app->get('/settings/user/password', function () use ($app) {
    $app['session']->set('_token', bin2hex(random_bytes(32)));

    return $app['twig']->render('backend.twig', array(
        'title' => 'Passwort ändern'
    ));
})
    ->bind('settings.user.password')
    ->before($logincheck)
;

$app->post('/settings/user/password', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $validate = array(
        'password' => $request->get('npw')
    );

    $constraint = new \Symfony\Component\Validator\Constraints\Collection(array(
        'password' => new \Symfony\Component\Validator\Constraints\Length(array(
            'min' => '10'
        ))
    ));

    $errors = $app['validator']->validateValue($validate, $constraint);

    $username = $app['session']->get('Username');
    $pwold = hash('whirlpool', $request->get('pwold') . $username . $app['secret_salt']);
    $pwnew = hash('whirlpool', $request->get('npw') . $username . $app['secret_salt']);

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (isset($ptoken) && isset($stoken) && $ptoken == $stoken) {
        $sql_gu = "SELECT `ID`, `Username`, `Password`FROM `User` WHERE `Username` = ?";
        $get_user = $app['db']->fetchAssoc($sql_gu, array((string) $username));

        if ($get_user['Password'] == $pwold) {

            if (count($errors) > 0) {
                $app['session']->getFlashBag()->add('Error', 'Das Passwort muss mindestens 10 Zeichen lang sein!');
                return $app->redirect($app['url_generator']->generate('settings.user.password'));
            } else {
                $app['db']->update('User', array(
                    'Password' => $pwnew
                ), array(
                    'Password' => $pwold
                ));

                $app['session']->getFlashBag()->add('Success', 'Du hast dein Passwort erfolgreich geändert!');
                return $app->redirect($app['url_generator']->generate('settings.user.password'));
            }
        } else {
            $app['session']->getFlashBag()->add('Error', 'Das alte Passwort ist nicht korrekt!');
            return $app->redirect($app['url_generator']->generate('settings.user.password'));
        }
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('settings.user.password'));
    }
})
    ->bind('settings.user.password.change')
    ->before($logincheck)
;

$app->get('/backend/support', function () use ($app) {
    $app['session']->set('_token', bin2hex(random_bytes(32)));

    $sql_gc = "SELECT * FROM `Contact` ORDER BY `ID` DESC";

    $get_contact = $app['db']->fetchAll($sql_gc);

    return $app['twig']->render('backend.twig', array(
        'title' => 'Supportanfragen',
        'contact' => $get_contact
    ));
})
    ->bind('backend.contact')
    ->before($logincheck)
;

$app->get('/backend/support/delete/{id}/{user}/{token}', function ($id, $user, $token) use ($app) {
    if ($user == $app['session']->get('Username') && $token == $app['session']->get('_token')) {
        if ($id != "") {
            $app['db']->delete('Contact', array(
                'ID' => $id
            ));

            $app['session']->getFlashBag()->add('Success', 'Die Anfrage wurde erfolgreich gelöscht!');
            return $app->redirect($app['url_generator']->generate('backend.contact'));
        } else {
            $app['session']->getFlashBag()->add('Error', 'Es wurde keine ID angegeben!');
            return $app->redirect($app['url_generator']->generate('backend.contact'));
        }
    } else {
        $app['session']->getFlashBag()->add('Error', 'Du bist nicht berechtigt, diese Aktion auszuführen!');
        return $app->redirect($app['url_generator']->generate('backend.contact'));
    }
})
    ->bind('backend.contact.delete')
    ->before($logincheck)
;

$app->post('/backend/block', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $username = $app['session']->get('Username');
    $email = $request->get('email');
    $reason = $request->get('reason');
    $block_cb = $request->get('block_checkbox');

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (isset($ptoken) && isset($stoken) && $ptoken == $stoken) {
        if (!isset($block_cb)) {
            $block_date = $request->get('block_date');

            $app['db']->insert('Block', array(
                'Blocked_by' => $username,
                'Email' => $email,
                'Reason' => $reason,
                'Date' => date("d.m.Y, H:i:s"),
                'permanent' => "0",
                'blocked_until' => $block_date
            ));
        } else {
            $app['db']->insert('Block', array(
                'Blocked_by' => $username,
                'Email' => $email,
                'Reason' => $reason,
                'Date' => date("d.m.Y, H:i:s"),
                'permanent' => "1"
            ));
        }

        $app['session']->getFlashBag()->add('Success', 'Die E-Mail Adresse <b>' . $email . '</b> wurde erfolgreich blockiert!');
        return $app->redirect($app['url_generator']->generate('backend.block'));
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('backend.contact'));
    }
})
    ->bind('backend.contact.block')
    ->before($logincheck)
;

$app->get('/backend/block', function () use ($app) {
    $sql = "SELECT * FROM Block";
    $get_block = $app['db']->fetchAll($sql);

    return $app['twig']->render('backend.twig', array(
        'title' => 'Blockierte Adressen',
        'blocked' => $get_block
    ));
})
    ->bind('backend.block')
    ->before($logincheck)
;

$app->post('/backend/unblock', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $id = $request->get('bID');
    $email = $request->get('email');

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (isset($ptoken) && isset($stoken) && $ptoken == $stoken) {
        $app['db']->delete('Block', array(
            'ID' => $id
        ));

        $app['session']->getFlashBag()->add('Success', 'Die E-Mail Adresse <b>' . $email . '</b> wurde erfolgreich entsperrt!');
        return $app->redirect($app['url_generator']->generate('backend.block'));
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('backend.block'));
    }
})
    ->bind('backend.unblock')
    ->before($logincheck)
;

$app->get('/backend/contact/done/{id}/{user}/{token}', function ($id, $user, $token) use ($app) {
    if ($user == $app['session']->get('Username') && $token == $app['session']->get('_token')) {
        if ($id != "") {
            $sql_done = "UPDATE `Contact` SET `Status`= 'Erledigt' WHERE `ID` = ?";
            $app['db']->executeUpdate($sql_done, array((int) $id));

            $app['session']->getFlashBag()->add('Success', 'Die Anfrage wurde erfolgreich als erledigt makiert!');
            return $app->redirect($app['url_generator']->generate('backend.contact'));
        } else {
            $app['session']->getFlashBag()->add('Error', 'Es wurde keine ID angegeben!');
            return $app->redirect($app['url_generator']->generate('backend.contact'));
        }
    } else {
        $app['session']->getFlashBag()->add('Error', 'Du bist nicht berechtigt, diese Aktion auszuführen!');
        return $app->redirect($app['url_generator']->generate('backend.contact'));
    }
})
    ->bind('backend.contact.done')
;

$app->get('/backend/content', function () use ($app) {
    $sql_content = "SELECT `Text` FROM `Content` WHERE `Definer` = 'about_me'";
    $get_content = $app['db']->fetchAssoc($sql_content);

    return $app['twig']->render('backend.twig', array(
        'title' => 'Inhalt',
        'content' => $get_content
    ));
})
    ->bind('backend.content')
    ->before($logincheck)
    ->before($admincheck)
;

$app->post('/backend/content', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $text = $request->get('text');

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (isset($ptoken) && isset($stoken) && $ptoken == $stoken) {
        $sql_chct = "UPDATE `Content` SET `Text`= ? WHERE `Definer` = ?";
        $app['db']->executeUpdate($sql_chct, array((string) $text, (string) 'about_me'));

        $app['session']->getFlashBag()->add('Success', 'Der Text wurde erfolgreich geändert!');
        return $app->redirect($app['url_generator']->generate('backend.content'));
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('backend.content'));
    }
})
    ->bind('backend.content.post')
    ->before($logincheck)
    ->before($admincheck)
;

$app->get('/backend/gbuch', function () use ($app) {
    $sql_gb = "SELECT * FROM Guestbook ORDER BY Date DESC";
    $get_gb = $app['db']->fetchAll($sql_gb);

    return $app['twig']->render('backend.twig', array(
        'title' => 'Gästebuch',
        'content' => $get_gb
    ));
})
    ->bind('backend.gbuch')
    ->before($logincheck)
;

$app->get('/backend/gbuch/delete/{id}/{user}/{token}', function ($id, $user, $token) use ($app) {
    if ($user == $app['session']->get('Username') && $token == $app['session']->get('_token')) {
        if ($id != "") {
            $app['db']->delete('Guestbook', array(
                'ID' => $id
            ));

            $app['session']->getFlashBag()->add('Success', 'Der Eintrag wurde erfolgreich gelöscht!');
            return $app->redirect($app['url_generator']->generate('backend.gbuch'));
        } else {
            $app['session']->getFlashBag()->add('Error', 'Es wurde keine ID angegeben!');
            return $app->redirect($app['url_generator']->generate('backend.gbuch'));
        }
    } else {
        $app['session']->getFlashBag()->add('Error', 'Du bist nicht berechtigt, diese Aktion auszuführen!');
        return $app->redirect($app['url_generator']->generate('backend.gbuch'));
    }
})
    ->bind('backend.gbuch.delete')
    ->before($logincheck)
    ->before($admincheck)
;

$app->get('/backend/gbuch/val/{id}/{user}/{token}', function ($id, $user, $token) use ($app) {
    if ($user == $app['session']->get('Username') && $token == $app['session']->get('_token')) {
        if ($id != "") {
            $sql_chug = "UPDATE `Guestbook` SET `Verified`= ? WHERE `ID` = ?";
            $app['db']->executeUpdate($sql_chug, array((int) '1', (int) $id));

            $app['session']->getFlashBag()->add('Success', 'Der Eintrag wurde erfolgreich bestätigt!');
            return $app->redirect($app['url_generator']->generate('backend.gbuch'));
        } else {
            $app['session']->getFlashBag()->add('Error', 'Es wurde keine ID angegeben!');
            return $app->redirect($app['url_generator']->generate('backend.gbuch'));
        }
    } else {
        $app['session']->getFlashBag()->add('Error', 'Du bist nicht berechtigt, diese Aktion auszuführen!');
        return $app->redirect($app['url_generator']->generate('backend.gbuch'));
    }
})
    ->bind('backend.gbuch.val')
    ->before($logincheck)
    ->before($admincheck)
;

$app->get('/backend/news', function () use ($app) {
    $sql = "SELECT * FROM News ORDER BY ID DESC;";
    $get_news = $app['db']->fetchAll($sql);

    return $app['twig']->render('backend.twig', array(
        'title' => 'News',
        'news' => $get_news
    ));
})
    ->bind('backend.news')
    ->before($logincheck)
    ->before($admincheck)
;

$app->post('/backend/news/add', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $title = $request->get('title');
    $text = $request->get('text');
    $date = date("d.m.Y, H:i:s");
    $user = $app['session']->get('Username');

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (isset($ptoken) && isset($stoken) && $ptoken == $stoken) {
        if (!isset($text) || $text == "") {
            $app['session']->getFlashBag()->add('Error', 'Der Text darf nicht leer sein!');
            return $app->redirect($app['url_generator']->generate('backend.news'));
        }
        
        $app['db']->insert("News", array(
            'Added_By' => $user,
            'Title' => $title,
            'Text' => $text,
            'Date' => $date
        ));

        $app['session']->getFlashBag()->add('Success', 'Der Eintrag wurde erfolgreich hinzugefügt!');
        return $app->redirect($app['url_generator']->generate('backend.news'));
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('backend.news'));
    }
})
    ->bind('backend.news.add')
    ->before($logincheck)
    ->before($admincheck)
;

$app->post('/backend/news/edit', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $title = $request->get('title');
    $text = $request->get('text');
    $id = $request->get('id');
    $date = date("d.m.Y, H:i:s");
    $user = $app['session']->get('Username');

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (isset($ptoken) && isset($stoken) && $ptoken == $stoken) {
        if (!isset($text) || $text == "") {
            $app['session']->getFlashBag()->add('Error', 'Der Text darf nicht leer sein!');
            return $app->redirect($app['url_generator']->generate('backend.news'));
        }

        $sql_chug = "UPDATE `News` SET `Added_By`= ?, `Title` = ?, `Text` = ?, `Date` = ? WHERE `ID` = ?";
        $app['db']->executeUpdate($sql_chug, array((string) $user, (string) $title, (string) $text, (string) $date, (int) $id));

        $app['session']->getFlashBag()->add('Success', 'Der Eintrag wurde erfolgreich bearbeitet!');
        return $app->redirect($app['url_generator']->generate('backend.news'));
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('backend.news'));
    }
})
    ->bind('backend.news.edit')
    ->before($logincheck)
    ->before($admincheck)
;

$app->get('/backend/news/deleteall/{user}/{token}', function ($user, $token) use ($app) {
    if ($user == $app['session']->get('Username') && $token == $app['session']->get('_token')) {
        $app['db']->executeUpdate($app['db']->getDatabasePlatform()->getTruncateTableSQL('News', true));

        $app['session']->getFlashBag()->add('Success', 'Die Einträge wurden erfolgreich gelöscht!');
        return $app->redirect($app['url_generator']->generate('backend.news'));
    } else {
        $app['session']->getFlashBag()->add('Error', 'Du bist nicht berechtigt, diese Aktion auszuführen!');
        return $app->redirect($app['url_generator']->generate('backend.news'));
    }
})
    ->bind('backend.news.deleteAll')
    ->before($logincheck)
    ->before($admincheck)
;

$app->get('/backend/news/delete/{id}/{user}/{token}', function ($id, $user, $token) use ($app) {
    if ($user == $app['session']->get('Username') && $token == $app['session']->get('_token')) {
        if ($id != "") {
            $app['db']->delete('News', array(
                'ID' => $id
            ));

            $app['session']->getFlashBag()->add('Success', 'Der Eintrag wurde erfolgreich gelöscht!');
            return $app->redirect($app['url_generator']->generate('backend.news'));
        } else {
            $app['session']->getFlashBag()->add('Error', 'Es wurde keine ID angegeben!');
            return $app->redirect($app['url_generator']->generate('backend.news'));
        }
    } else {
        $app['session']->getFlashBag()->add('Error', 'Du bist nicht berechtigt, diese Aktion auszuführen!');
        return $app->redirect($app['url_generator']->generate('backend.news'));
    }
})
    ->bind('backend.news.delete')
    ->before($logincheck)
    ->before($admincheck)
;

$app->get('/backend/gbuch/unval/{id}/{user}/{token}', function ($id, $user, $token) use ($app) {
    if ($user == $app['session']->get('Username') && $token == $app['session']->get('_token')) {
        if ($id != "") {
            $sql_chug = "UPDATE `Guestbook` SET `Verified`= ? WHERE `ID` = ?";
            $app['db']->executeUpdate($sql_chug, array((int) '0', (int) $id));

            $app['session']->getFlashBag()->add('Success', 'Der Eintrag wurde erfolgreich ausgeblendet!');
            return $app->redirect($app['url_generator']->generate('backend.gbuch'));
        } else {
            $app['session']->getFlashBag()->add('Error', 'Es wurde keine ID angegeben!');
            return $app->redirect($app['url_generator']->generate('backend.gbuch'));
        }
    } else {
        $app['session']->getFlashBag()->add('Error', 'Du bist nicht berechtigt, diese Aktion auszuführen!');
        return $app->redirect($app['url_generator']->generate('backend.gbuch'));
    }
})
    ->bind('backend.gbuch.unval')
    ->before($logincheck)
    ->before($admincheck)
;

$app->get('/backend/count/{type}', function ($type) use ($app) {
    switch ($type) {
        case "all":
            $sel_gb = "SELECT COUNT(*) AS Anzahl FROM Guestbook WHERE Verified = '0'";
            $gb_ct = $app['db']->fetchAssoc($sel_gb);
            $sel_sup = "SELECT COUNT(*) AS Anzahl FROM Contact WHERE Status = 'Offen'";
            $sup = $app['db']->fetchAssoc($sel_sup);

            return $gb_ct['Anzahl'] + $sup['Anzahl'];
            break;
        case "gbuch":
            $sel_gb = "SELECT COUNT(*) AS Anzahl FROM Guestbook WHERE Verified = '0'";
            $gb_ct = $app['db']->fetchAssoc($sel_gb);
            return $gb_ct['Anzahl'];
            break;
        case "sup":
            $sel_sup = "SELECT COUNT(*) AS Anzahl FROM Contact WHERE Status = 'Offen'";
            $sup = $app['db']->fetchAssoc($sel_sup);
            return $sup['Anzahl'];
            break;
    }
})
    ->before($logincheck)
;

$app->get("/backend/teamspeak", function () use ($app) {
    return $app['twig']->render("backend.twig", array(
        'title' => 'Teamspeak'
    ));
})
    ->bind("backend.teamspeak")
    ->before($logincheck)
;

$app->get("/backend/teamspeak/set", function () use ($app) {
    require_once("libraries/TeamSpeak3/TeamSpeak3.php");
    require_once ("data.php");

    $user = $app['session']->get("Username");
    $group = $app['session']->get("Group");
    $uID = $app['session']->get("uID");

    try {
        $ts3_VirtualServer = TeamSpeak3::factory("serverquery://" . ts_user . ":" . ts_pass . "@127.0.0.1:10011/?server_port=9987");
        $client = $ts3_VirtualServer->clientFindDb($user, false);

        if ($group == "Administrator") {
            $ts3_VirtualServer->serverGroupClientAdd(6, $client[0]);
            $ts3_VirtualServer->serverGroupClientAdd(9, $client[0]);

            $app['db']->insert('Teamspeak', array(
                'clientID' => $client,
                'uID' => $uID,
                'ServerAdmin' => 1,
                'Moderator' => 1
            ));
        }

        if ($group == "Mod") {
            $ts3_VirtualServer->serverGroupClientAdd(9, $client[0]);

            $app['db']->insert('Teamspeak', array(
                'clientID' => $client,
                'uID' => $uID,
                'ServerAdmin' => 0,
                'Moderator' => 1
            ));
        }

        $app['session']->getFlashBag()->add('Success', 'Dir wurden die Rechte erfolgreich zugewiesen!');
        return $app->redirect($app['url_generator']->generate("backend.teamspeak"));
    }
    catch (Exception $e) {
        if ($e->getMessage() == "database empty result set") {
            $app['session']->getFlashBag()->add('Error', 'Der Benutzer <b>' . $user . '</b> wurde auf dem Teamspeak nicht gefunden!<br /> Bitte stelle sicher, dass dein Benutzername auf dem Teamspeak mit dem von der Website übereinstimmt!');
            return $app->redirect($app['url_generator']->generate("backend.teamspeak"));
        } elseif ($e->getMessage() == "duplicate entry") {
            $app['session']->getFlashBag()->add('Error', 'Du besitzt bereits deine Rechte!');
            return $app->redirect($app['url_generator']->generate("backend.teamspeak"));
        }
    }
})
    ->bind("backend.teamspeak.set")
    ->before($logincheck)
;

$app->get("/backend/wiki", function () use ($app) {
    $sql = "SELECT * FROM Wiki";
    $wiki = $app['db']->fetchAll($sql);

    return $app['twig']->render("backend.twig", array(
        "title" => "Wiki",
        "wiki" => $wiki
    ));
})
    ->bind("backend.wiki")
    ->before($logincheck)
;

$app->get('/backend/user/change_email', function () use ($app) {
    $sql = "SELECT u.Username, u.Email FROM User u WHERE u.Username = ? LIMIT 1";
    $get_user = $app['db']->fetchAssoc($sql, array((string) $app['session']->get('Username')));

    $app['session']->set('backend.user.email.change.email_address', $get_user['Email']);

    return $app['twig']->render('backend.twig', array(
        'title' => 'E-Mail Adresse ändern',
        'email_address' => $app['session']->get('backend.user.email.change.email_address')
    ));
})
    ->bind('backend.user.change_email')
    ->before($logincheck)
;

$app->post('/backend/user/change_email', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $username = $app['session']->get('Username');
    $email = $app['session']->get('backend.user.email.change.email_address');
    $email_new = $request->get('email_new');

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (isset($ptoken, $stoken) && $ptoken == $stoken) {
        if (!isset($email)) {
            $app['session']->getFlashBag()->add('Error', 'Bitte die E-Mail Adresse angeben!');
            return $app->redirect($app['url_generator']->generate('backend.user.change_email'));
        }

        /**
         * E-Mail Adresse validieren
         */

        $val_email = $app['validator']->validate($email_new, new \Symfony\Component\Validator\Constraints\Email());

        if (count($val_email) > 0) {
            $app['session']->getFlashBag()->add('Error', 'Die eingegebene E-Mail Adresse ist nicht gültig!');
            return $app->redirect($app['url_generator']->generate('backend.user.change_email'));
        }

        if ($email == $email_new) {
            $app['session']->getFlashBag()->add('Error', 'Die E-Mail Adressen dürfen nicht übereinstimmen!');
            return $app->redirect($app['url_generator']->generate('backend.user.change_email'));
        }

        $update_email_sql = "UPDATE User SET Email = ? WHERE Username = ?";
        $app['db']->executeUpdate($update_email_sql, array(
            (string) $email_new,
            (string) $username
        ));

        $app['session']->getFlashBag()->add('Success', 'Deine E-Mail Adresse wurde erfolgreich von <b>' . $email . '</b> zu <b>' . $email_new . '</b> geändert!');
        return $app->redirect($app['url_generator']->generate('backend.user.change_email'));
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('backend.user.change_email'));
    }
})
    ->bind('backend.user.change_email.post')
    ->before($logincheck)
;