<?php
/**
 * Created by PhpStorm.
 * User: fthielen
 * Date: 05.12.2016
 * Time: 16:27
 */

require_once("twitteroauth/twitteroauth/twitteroauth.php");
require_once 'TweetPHP.php';
require_once __DIR__ . '/../data.php';

$TweetPHP = new TweetPHP(array(
    'consumer_key' => CONSUMER_KEY,
    'consumer_secret' => CONSUMER_SECRET,
    'access_token' => ACCESS_TOKEN,
    'access_token_secret' => ACCESS_TOKEN_SECRET,
    'twitter_screen_name' => 'thamianplay',
    'tweets_to_retrieve' => 20,
    'tweets_to_display' => 5
));

$date_format = "d.m.Y, H:i:s";
$twitter_style_dates = true;

if (!function_exists('form_tw_time')) {
    function form_tw_time($tstamp, $twitter_style_dates, $date_format) {
        $tweet_time = strtotime($tstamp);

        if ($twitter_style_dates == true){
            // Current UNIX timestamp.
            $current_time = time();
            $time_diff = abs($current_time - $tweet_time);
            switch ($time_diff)
            {
                case ($time_diff < 60):
                    $display_time = $time_diff.' seconds ago';
                    break;
                case ($time_diff >= 60 && $time_diff < 3600):
                    $min = floor($time_diff/60);
                    $display_time =  "Vor " .  $min.' Minuten';
                    break;
                case ($time_diff >= 3600 && $time_diff < 86400):
                    $hour = floor($time_diff/3600);
                    $display_time = 'Vor '.$hour.' Stunde';
                    if ($hour > 1){ $display_time .= 'n'; }
                    break;
                case ($time_diff >= 86400):
                    $days = floor(($time_diff / 3600) / 24);
                    if ($days > 1) {
                        $display_time = "Vor " . $days . " Tagen";
                    } else {
                        $display_time = "Vor einem Tag";
                    }
                    break;
                default:
                    $display_time = date($date_format,$tweet_time);
                    break;
            }
        } else {
            $display_time = date($date_format,$tweet_time);
        }

        return $display_time;
    }
}

$tweets = array();

$the_tweets = $TweetPHP->get_tweet_array();

for ($i = 0; $i < count($the_tweets); $i++) {
    $tweets[] = array(
        'text' => $TweetPHP->autolink($the_tweets[$i]["text"]),
        'datum' => form_tw_time($the_tweets[$i]["created_at"], true, 'd.m.Y, H:i:s'),
        'id' => $the_tweets[$i]["id_str"],
        'fav_count' => $the_tweets[$i]["favorite_count"]
    );
}