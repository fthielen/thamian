<?php
/**
 * Created by IntelliJ IDEA.
 * User: w10
 * Date: 5/1/2016
 * Time: 12:59 AM
 */

/**
 * Login GET Route
 */

$app->get('/login', function () use ($app) {
    $app['session']->set('_token', bin2hex(openssl_random_pseudo_bytes(16)));

    return $app['twig']->render('devmode.twig', array(
        'title' => 'Login',
        'csrf_token' => $app['session']->get('_token')
    ));
})->bind('devlogin');

/**
 * Login Post Route
 */

$app->post('/login', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $username = $request->get('username');
    $password = hash('whirlpool', $request->get('password') . $username . $app['secret_salt']);

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (isset($ptoken, $stoken) && $ptoken == $stoken) {
        $sql = "SELECT u.`ID`, u.`Username`, u.`Password`, g.`Name` FROM `User` u JOIN `Groups` g ON u.gID = g.ID WHERE Username = ?";
        $get_user = $app['db']->fetchAssoc($sql, array((string) $username));

        if ($get_user['Password'] == $password) {
            $app['session']->set('Username', $get_user['Username']);
            $app['session']->set('Group', $get_user['Name']);
            $app['session']->set('uID', $get_user['ID']);

            $update = "UPDATE `User` SET `Last_Login` = ? WHERE `Username` = ?";
            $app['db']->executeUpdate($update, array((string) date("d.m.Y, H:i:s"), (string) $username));

            $app['session']->getFlashBag()->add('Success', 'Erfolgreich angemeldet!');

            return $app->redirect('/');
        } else {
            $app['session']->getFlashBag()->add('Error', 'Benutzername und/oder Passwort falsch!');
            return $app->redirect($app['url_generator']->generate('devlogin'));
        }
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('devlogin'));
    }
})
    ->bind('devlogin_post')
;

/**
 * Logout GET Route
 */

$app->get('/logout', function () use ($app) {
    $app['session']->clear();

    $app['session']->getFlashBag()->add('Success', 'Erfolgreich abgemeldet!');

    return $app->redirect('/');
})->bind('logout');

/**
 * Passwort vergessen
 */

/**
 * Passwort zurücksetzen GET Route
 */

$app->get('/login/password_reset', function () use ($app) {
    $app['session']->set('_token', bin2hex(openssl_random_pseudo_bytes(16)));

    return $app['twig']->render('devmode.twig', array(
        'title' => 'Passwort zurücksetzen',
        'csrf_token' => $app['session']->get('_token')
    ));
})
    ->bind('login.password.reset.get')
;

/**
 * Passwort zurücksetzen POST Route
 */

$app->post('/login/password_reset', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $username = $request->get('username');
    $email = $request->get('email');

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (isset($ptoken, $stoken) && $ptoken == $stoken) {
        if (!isset($username, $email)) {
            $app['session']->getFlashBag()->add('Error', 'Bitte alle Felder ausfüllen!');
            return $app->redirect($app['url_generator']->generate('login.password.reset.get'));
        }

        $sql = "SELECT u.ID, u.Username, u.Email FROM User u WHERE u.Username = ? LIMIT 1";
        $get_user = $app['db']->fetchAssoc($sql, array((string) $username));
        $get_c = $app['db']->fetchAll($sql, array((string) $username));

        if (count($get_c) != 1) {
            $app['session']->getFlashBag()->add('Error', 'Dieser Benutzer existiert nicht!');
            return $app->redirect($app['url_generator']->generate('login.password.reset.get'));
        }

        if ($get_user['Email'] != $email) {
            $app['session']->getFlashBag()->add('Error', 'Die E-Mail Adresse ist nicht korrekt!');
            return $app->redirect($app['url_generator']->generate('login.password.reset.get'));
        }

        $reset_token = bin2hex(openssl_random_pseudo_bytes(16));

        require_once 'data.php';

        $app['db']->insert('Password_Reset', array(
            'Reset_Hash' => $reset_token,
            'Reset_Date' => date("d.m.Y, H:i:s"),
            'uID' => $get_user['ID']
        ));

        /**
         * Mail versenden
         */

        $mail_body = '
            <html>
                <p>Hi ' . $username . ',</p>
                <p>Du erhälst diese E-Mail, da die "Password zurücksetzen" Funktion auf der Seite thamian.de verwendet wurde.</p>
                <p>Wenn du dein Passwort zurücksetzen möchtest, klicke bitte auf folgenden Link (oder kopiere diesen in deinen Browser):</p>
                <p><a href="https://www.thamian.de/login/reset/' . $reset_token . '">https://www.thamian.de/login/reset/' . $reset_token . '</a></p>
                <p>Du hast diese Passwortänderung nicht angefordert?</p>
                <p>Dann ignoriere diese E-Mail einfach.</p>
            </html>
        ';

        $mail = new PHPMailer();

        $mail->CharSet = "UTF-8";

        $mail->isSMTP();
        $mail->Host = mail_host;
        $mail->SMTPAuth = true;
        $mail->Username = mail_username;
        $mail->Password = mail_pass;
        $mail->SMTPSecure = "tls";
        $mail->Port = 587;

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->setFrom('info@thamian.de', 'Thamian.de');
        $mail->addAddress($email);
        $mail->isHTML(true);

        $mail->Subject = "Thamian.de Passwort zurücksetzen";
        $mail->Body = $mail_body;

        if (!$mail->send()) {
            $app['session']->getFlashBag()->add('Error', 'Fehler beim senden der E-Mail!');
            return $app->redirect($app['url_generator']->generate('login.password.reset.get'));
        }

        /**
         * Alles erfolgreich --> Zurück leiten
         */

        $app['session']->getFlashBag()->add('Success', 'Eine E-Mail zum zurücksetzen des Passworts wurde versendet, bitte prüfe dein E-Mail Postfach!');
        return $app->redirect($app['url_generator']->generate('login.password.reset.get'));
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect($app['url_generator']->generate('login.password.reset.get'));
    }
})
    ->bind('login.password.reset.post')
;

/**
 * Passwort von E-Mail zurücksetzen GET Route
 */

$app->get('/login/reset/{reset_token}', function ($reset_token) use ($app) {
    if (!isset($reset_token)) {
        $app['session']->getFlashBag()->add('Error', 'Ein Token wird benötigt!');
        return $app->redirect($app['url_generator']->generate('login.password.reset.get'));
    }

    $sql = "SELECT r.Reset_Hash, u.Username FROM Password_Reset r JOIN User u ON r.uID = u.ID WHERE r.Reset_Hash = ? LIMIT 1";
    $get_user = $app['db']->fetchAssoc($sql, array((string) $reset_token));
    $get_c = $app['db']->fetchAll($sql, array((string) $reset_token));

    if (count($get_c) != 1) {
        $app['session']->getFlashBag()->add('Error', 'Dieser Token ist nicht korrekt!');
        return $app->redirect($app['url_generator']->generate('login.password.reset.get'));
    }

    $app['session']->set('_token', bin2hex(openssl_random_pseudo_bytes(16)));
    $app['session']->set('password_reset_token', $reset_token);
    $app['session']->set('password_reset_user', $get_user['Username']);

    return $app['twig']->render('devmode.twig', array(
        'title' => 'Passwort ändern',
        'username' => $get_user['Username'],
        'reset_token' => $reset_token,
        'csrf_token' => $app['session']->get('_token')
    ));
})
    ->bind('login.password.reset.getByMail')
;

$app->post('/login/reset', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    require_once 'data.php';

    $username = $request->get('username');
    $email = $request->get('email');
    $new_password = $request->get('new_password');
    $hashed_password = hash('whirlpool', $request->get('new_password') . $username . $app['secret_salt']);

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    $reset_token = $app['session']->get('password_reset_token');

    if (isset($ptoken, $stoken) && $ptoken == $stoken) {
        if (!isset($username, $email, $new_password)) {
            $app['session']->getFlashBag()->add('Error', 'Bitte alle Felder ausfüllen!');
            return $app->redirect('https://www.thamian.de/login/reset/' . $reset_token);
        }

        /**
         * Validate Passwort
         */

        $pw_val = $app['validator']->validate(array(
            'password' => $new_password
        ), new \Symfony\Component\Validator\Constraints\Collection(array(
            'password' => new \Symfony\Component\Validator\Constraints\Length(array(
                'min' => '10'
            ))
        )));

        if (count($pw_val) > 0) {
            $app['session']->getFlashBag()->add('Error', 'Das Passwort muss mindestens 10 Zeichen lang sein!');
            return $app->redirect('https://www.thamian.de/login/reset/' . $reset_token);
        }

        /**
         * Stimmt der Benutzer überein?
         */

        if ($app['session']->get('password_reset_user') != $username) {
            $app['session']->getFlashBag()->add('Error', 'Der Benutzer stimmt nicht überein!');
            return $app->redirect('https://www.thamian.de/login/reset/' . $reset_token);
        }

        /**
         * Benutzer in der Datenbank prüfen
         */

        $sql = "SELECT u.Username, u.Password, u.Email FROM User u WHERE u.Username = ? LIMIT 1";
        $get_user = $app['db']->fetchAssoc($sql, array((string) $username));

        if ($email != $get_user['Email']) {
            $app['session']->getFlashBag()->add('Error', 'Die E-Mail Adresse ist nicht korrekt!');
            return $app->redirect('https://www.thamian.de/login/reset/' . $reset_token);
        }

        /**
         * Passwort aktualisieren
         */

        $update_user_sql = "UPDATE User SET Password = ? WHERE Username = ?";
        $app['db']->executeUpdate($update_user_sql, array(
            $hashed_password,
            (string) $username
        ));

        /**
         * Token aus der Datenbank löschen
         */

        $app['db']->delete('Password_Reset', array(
            'Reset_Hash' => $app['session']->get('password_reset_token')
        ));

        $app['session']->getFlashBag()->add('Success', 'Dein Passwort wurde erfolgreich zurückgesetzt!');
        return $app->redirect($app['url_generator']->generate('devlogin'));
    } else {
        $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
        return $app->redirect('https://www.thamian.de/login/reset/' . $reset_token);
    }
})
    ->bind('login.password.reset.postByMail')
;