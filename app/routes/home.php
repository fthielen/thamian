<?php
/**
 * Created by IntelliJ IDEA.
 * User: w10
 * Date: 4/30/2016
 * Time: 1:43 AM
 */

$block_check = function (\Symfony\Component\HttpFoundation\Request $request, \Silex\Application $app) {
    $email = $request->get('email');

    if (isset($email) && $email != "") {
        $sel = "SELECT * FROM Block WHERE Email = ?";
        $get_c = $app['db']->fetchAll($sel, array((string) $email));
        $get_em = $app['db']->fetchAssoc($sel, array((string) $email));

        if (count($get_c) > 0) {
            $reason = $get_em['Reason'];
            $perma = $get_em['permanent'];

            if ($perma == true) {
                $app['session']->getFlashBag()->add('Error', 'Deine E-Mail Adresse wurde permanent aus dem Grund <b>' . $reason . '</b> gesperrt!');
                return $app->redirect($app['url_generator']->generate('home'));
            } else {
                $date_until = $get_em['blocked_until'];
                $date_today = date("d.m.Y");

                if (strtotime($date_until) > strtotime($date_today)) {
                    $app['session']->getFlashBag()->add('Error', 'Deine E-Mail Adresse wurde bis zum ' . $date_until . ' aus dem Grund <b>' . $reason . '</b> gesperrt!');
                    return $app->redirect($app['url_generator']->generate('home'));
                }
            }
        }
    }
};

$app->error(function (\Exception $e, $code) use($app) {
    switch ($code) {
        case 404:
            return $app['twig']->render('error.twig', array(
                'title' => '404 - Seite nicht gefunden'
            ));
            break;
        default:
            return $app['twig']->render('error.twig', array(
                'title' => '500 - Serverfehler',
                'status' => $e
            ));
    }
});

$app->get('/', function () use ($app) {
    /**
     * News w/o Angular
     */

    $sql = "SELECT * FROM News ORDER BY ID DESC";
    $get_news = $app['db']->fetchAll($sql);

    /**
     * Get Tweets
     */

    include __DIR__ . '/Twitter/tweets.php';

    return $app['twig']->render('home.twig', array(
        'title' => 'Home',
        'news' => $get_news,
        'tweets' => $tweets
    ));
})->bind('home');

$app->get('/stream/json', function () use ($app) {
    $channel = "thamianplay";
    $thamiid = "102325790";

    /**
     * Twitch Past Broadcast
     */

    $past_broadcasts = json_decode(@file_get_contents('https://api.twitch.tv/kraken/channels/' . $channel . '/videos?broadcasts=true&client_id=' . clientID), true);

    /**
     * Twitch Online Status
     */

    $channels = array($channel) ;
    $callAPI = implode(",",$channels);
    $dataArray = json_decode(@file_get_contents('https://api.twitch.tv/kraken/streams?channel=' . $callAPI . '&client_id=' . clientID), true);

    foreach($dataArray['streams'] as $mydata){
        if($mydata['_id'] != null){
            $name 		= $mydata['channel']['display_name'];
            $game		= $mydata['channel']['game'];
            $url		= $mydata['channel']['url'];
            $viewer		= $mydata['viewers'];
            $title = $mydata['channel']['status'];

            $status = "online";
        } elseif ($mydata['_id'] == null) {
            $status = "offline";
        }
    }

    if (isset($status) && $status == "online") {
        return json_encode(array_merge($mydata, array('is_offline' => false)));
    } else {
        $ch_offline = json_decode(@file_get_contents("https://api.twitch.tv/kraken/channels/" . $channel . '?client_id=' . clientID), true);

        $host = json_decode(@file_get_contents("http://tmi.twitch.tv/hosts?include_logins=1&host=" . $thamiid . '&client_id=' . clientID), true);
        $h_channel = $host['hosts'][0]['target_login'];

        $data = array(
            'offline_img' => $ch_offline['video_banner'],
            'hosted_channel' => $h_channel,
            'is_offline' => true
        );

        if (isset($h_channel)) {
            $data = array_merge($data, array('is_hosting' => true));
        } else {
            $data = array_merge($data, array('is_hosting' => false));
        }

        $data = array_merge($data, array('_total' => $past_broadcasts['_total']));

        return json_encode(array_merge($data, array('past_broadcast' => $past_broadcasts['videos'][0])));
    }
});

$app->get('/news/json', function () use ($app) {
    $sql = "SELECT * FROM News ORDER BY ID DESC";
    $get_news = $app['db']->fetchAll($sql);

    header('Content-type: application/json');

    return json_encode($get_news);
});

$app->get('/news/twitch/json', function () use ($app) {
    /**
     * Twitch Channel Feed
     */

    require_once 'data.php';

    $curl = curl_init();

    $headers = array(
        'Client-ID: ' . clientID
    );

    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_HEADER => $headers,
        CURLOPT_URL => 'https://api.twitch.tv/kraken/feed/thamianplay/posts?client_id=' . clientID
    ));

    $response = curl_exec($curl);

    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    $body = json_decode(substr($response, $header_size), true);

    /**
     * Datum konvertieren
     */
    $twitch_news = array();
    foreach ($body['posts'] as $key=>$eintrag)
    {
        $tw_datum = $eintrag['created_at'];
        $new_date = DateTime::createFromFormat(DateTime::ISO8601, $tw_datum);
        $new_date = $new_date->format('d.m.Y, H:i:s');
        $twitch_news[$key] = array_replace($body["posts"][$key], array(
            'created_at' => $new_date
        ));
    }

    return json_encode($twitch_news);
});

$app->get('/contact', function () use ($app) {
    $app['session']->set('_token', bin2hex(random_bytes(32)));

    return $app['twig']->render('home.twig', array(
        'title' => 'Kontakt'
    ));
})->bind('contact');

$app->post('/contact', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $name = $request->get('name');
    $email = $request->get('email');
    $subject = $request->get('subject');
    $text = $request->get('text');
    $datum = date("d.m.Y, H:i:s");

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    if (!isset($name, $email, $subject, $text) || $text == "") {
        $app['session']->getFlashBag()->add("Error", "Bitte alle Felder ausfüllen!");
        return $app->redirect($app['url_generator']->generate('contact'));
    }

    $constrain = new \Symfony\Component\Validator\Constraints\Collection(array(
        'name' => new \Symfony\Component\Validator\Constraints\Length(array(
            'min' => '5',
            'max' => '40'
        )),
        'email' => new \Symfony\Component\Validator\Constraints\Email(),
        'subject' => new \Symfony\Component\Validator\Constraints\Length(array(
            'min' => '5',
            'max' => '40'
        )),
        'text' => new \Symfony\Component\Validator\Constraints\Length(array(
            'min' => '10'
        ))
    ));

    $errors = $app['validator']->validateValue($email, new \Symfony\Component\Validator\Constraints\Email());

    if (count($errors) > 0) {
        $app['session']->getFlashBag()->add('Error', 'Diese E-Mail Adresse ist nicht gültig!');
        return $app->redirect($app['url_generator']->generate('contact'));
    } else {
        if (isset($ptoken) && isset($stoken) && $ptoken == $stoken) {
            $app['db']->insert('Contact', array(
                'Name' => $name,
                'Email' => $email,
                'Subject' => $subject,
                'Message' => $text,
                'Date' => $datum,
                'Status' => 'Offen'
            ));

            $app['session']->getFlashBag()->add('Success', 'Deine Anfrage wurde erfolgreich abgeschickt!');
            return $app->redirect($app['url_generator']->generate('contact'));
        } else {
            $app['session']->getFlashBag()->add("Error", "CSRF Token mismatch!");
            return $app->redirect($app['url_generator']->generate('contact'));
        }
    }
})
    ->bind('contact.post')
    ->before($block_check)
;

$app->get('/about', function () use ($app) {
    $sql_content = "SELECT `Text` FROM `Content` WHERE `Definer` = 'about_me'";
    $get_content = $app['db']->fetchAssoc($sql_content);

    return $app['twig']->render('home.twig', array(
        'title' => 'Über mich',
        'Content' => $get_content
    ));
})
    ->bind('about')
;

$app->get('/gbuch', function () use ($app) {
    $sql_gb = "SELECT * FROM Guestbook WHERE `Verified` = '1' ORDER BY Date DESC";
    $get_gb = $app['db']->fetchAll($sql_gb);

    return $app['twig']->render('home.twig', array(
        'title' => 'Gästebuch',
        'content' => $get_gb
    ));
})
    ->bind('gbuch')
;

$app->post('/gbuch', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
    $title = $request->get('title');
    $name = $request->get('name');
    $email = $request->get('email');
    $text = $request->get('text');
    $datum = date("d.m.Y, H:i:s");

    $ptoken = $request->get('csrf_token');
    $stoken = $app['session']->get('_token');

    $errors = $app['validator']->validateValue($email, new \Symfony\Component\Validator\Constraints\Email());

    if (!isset($title, $name, $email, $text) || $text == "") {
        $app['session']->getFlashBag()->add("Error", "Bitte alle Felder ausfüllen!");
        return $app->redirect($app['url_generator']->generate('gbuch'));
    }

    if (count($errors) > 0) {
        $app['session']->getFlashBag()->add('Error', 'Diese E-Mail Adresse ist nicht gültig!');
        return $app->redirect($app['url_generator']->generate('contact'));
    } else {
        if (isset($ptoken) && isset($stoken) && $ptoken == $stoken) {
            $app['db']->insert('Guestbook', array(
                'Title' => $title,
                'Name' => $name,
                'Email' => $email,
                'Text' => $text,
                'Date' => $datum,
                'Verified' => '0'
            ));

            $app['session']->getFlashBag()->add('Success', 'Der Eintrag wurde erfolgreich abgeschickt!');
            return $app->redirect($app['url_generator']->generate('gbuch'));
        } else {
            $app['session']->getFlashBag()->add("Error", "CSRF Token mismatch!");
            return $app->redirect($app['url_generator']->generate('gbuch'));
        }
    }
})
    ->bind('gbuch.insert')
    ->before($block_check)
;

$app->get("/sess_check", function () use ($app) {
    if (null === $user = $app['session']->get('Username')) {
        return "false";
    } else {
        return "true";
    }
})
    ->bind('sess_check')
;

$app->get("/server/teamspeak", function () use ($app) {
    require_once("libraries/TeamSpeak3/TeamSpeak3.php");
    require_once ("data.php");

    $ts3_VirtualServer = TeamSpeak3::factory("serverquery://" . ts_user . ":" . ts_pass . "@127.0.0.1:10011/?server_port=9987");
    $viewer = $ts3_VirtualServer->getViewer(new TeamSpeak3_Viewer_Html("/images/viewer/", "/images/flags/", "data:image"));

    return $app['twig']->render('home.twig', array(
        'title' => 'Teamspeak',
        'viewer' => $viewer
    ));
})
    ->bind("server.teamspeak")
;

$app->get("/server/teamspeak/viewer", function () use ($app) {
    require_once("libraries/TeamSpeak3/TeamSpeak3.php");
    require_once ("data.php");
    
    $ts3_VirtualServer = TeamSpeak3::factory("serverquery://" . ts_user . ":" . ts_pass . "@127.0.0.1:10011/?server_port=9987");
    $viewer = $ts3_VirtualServer->getViewer(new TeamSpeak3_Viewer_Html("/images/viewer/", "/images/flags/", "data:image"));

    return $viewer;
});

$app->get("/server/gaming", function () use ($app) {
    return $app['twig']->render('home.twig', array(
        'title' => 'Gamingserver'
    ));
})
    ->bind("server.gaming")
;