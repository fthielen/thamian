<?php
/**
 * Created by IntelliJ IDEA.
 * User: fthielen
 * Date: 06.09.2016
 * Time: 15:55
 */
namespace Thamian;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;

class Pusher implements WampServerInterface
{
    /**
     * @var array
     * Subscribed Topics
     */

    protected $subscribedTopics = array();

    public function onSubscribe(ConnectionInterface $conn, $topic)
    {
        $this->subscribedTopics[$topic->getId()] = $topic;
    }

    /**
     * @param $entry JSON News from ZeroMQ
     */

    public function onNewsEntry($entry)
    {
        $entryData = json_decode($entry, true);

        if (!array_key_exists($entryData['Title'], $this->subscribedTopics)) {
            return;
        }

        $topic = $this->subscribedTopics[$entryData['Title']];

        $topic->broadcast($entryData);
    }

    public function onUnSubscribe(ConnectionInterface $conn, $topic)
    {
        // TODO: Implement onUnSubscribe() method.
    }

    public function onOpen(ConnectionInterface $conn)
    {
        // TODO: Implement onOpen() method.
    }

    public function onClose(ConnectionInterface $conn)
    {
        // TODO: Implement onClose() method.
    }

    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }

    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        $conn->close();
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        // TODO: Implement onError() method.
    }
}