/**
 * Created by w10 on 6/6/2016.
 */
var thamian = angular.module('thamianQuotes', []);

thamian.controller('QuotesController', function ($scope, $http) {
    $http.get("/bot/quotes/json").success(function (response) {
        $scope.quotes = response;
    });

    $http.get("/bot/quotes/count").success(function (response) {
        $scope.quoteCount = response;
    });

    $scope.getData = function () {
        $http.get("/bot/quotes/json").success(function (response) {
            $scope.quotes = response;
        });
    }

    $scope.getQuotes = function () {
        $http.get("/bot/quotes/count").success(function (response) {
            $scope.quoteCount = response;
        });
    }

    setInterval($scope.getData, 20000);
    setInterval($scope.getQuotes, 20000);
});