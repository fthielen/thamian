/**
 * Quelle der Funktion truncText: https://gist.github.com/ChrisCinelli/5688048
 * @param text
 * @param maxLength
 * @param ellipseText
 * @returns {*}
 */
function truncText (text, maxLength, ellipseText){
    ellipseText = ellipseText || '&hellip;';

    if (text.length < maxLength)
        return text;

    //Find the last piece of string that contain a series of not A-Za-z0-9_ followed by A-Za-z0-9_ starting from maxLength
    var m = text.substr(0, maxLength).match(/([^A-Za-z0-9_]*)[A-Za-z0-9_]*$/);
    if(!m) return ellipseText;

    //Position of last output character
    var lastCharPosition = maxLength-m[0].length;

    //If it is a space or "[" or "(" or "{" then stop one before.
    if(/[\s\(\[\{]/.test(text[lastCharPosition])) lastCharPosition--;

    //Make sure we do not just return a letter..
    return (lastCharPosition ? text.substr(0, lastCharPosition+1) : '') + ellipseText;
}

var thamian = angular.module('thamianStream', ['ui.bootstrap']);
var onl_status;

thamian.controller('StreamController', function ($scope, $http) {
    angular.element(document).ready(function () {
        $http.get("/stream/json").success(function (response) {
            if (response["is_offline"] == false) {
                //Stream online
                $(".h3_stream_status").html("Stream (<span class='stream-online'><i class='fa fa-video-camera' aria-hidden='true'></i> Online</span>)");
                $(".online_stream_stat").html("<b>" + response['channel']['display_name'] + "</b> ist online und spielt <b>" + response['game'] + "</b> für <b>" + response['viewers'] + "</b> Zuschauer.");
                $(".online_stream_title").html("<b>Status: </b>" + truncText(response['channel']['status'], 66));
                $(".online_stream_title").attr("title", response['channel']['status']);
                $(".online_stream_title").attr("tooltip", response['channel']['status']);
                $(".online_stream_embed").html("<iframe class='embed-responsive-item twitch-stream' data-status='online' src='https://player.twitch.tv/?channel=" + response['channel']['name'] + "&!autoplay' frameborder='0' scrolling='no'></iframe>");
                $(".watch_button").html("<a href='https://www.twitch.tv/" + response['channel']['name'] + "' target='_blank'><i class='fa fa-twitch' aria-hidden='true'></i> Auf Twitch gucken</a>");
                onl_status = "online";
            } else {
                //Stream offline
                if (response["is_hosting"] == true) {
                    //Offline, aber hostet
                    $(".h3_stream_status").html("Stream (<span class='stream-offline'>Offline</span>)");
                    $(".online_stream_title").html("");
                    $(".online_stream_stat").html("<b>ThamianPlay</b> ist offline und hostet <b><a href='https://www.twitch.tv/" + response['hosted_channel'] + "' target='_blank'>" + response['hosted_channel'] + "</a></b>:");
                    $(".online_stream_embed").html("<iframe class='embed-responsive-item twitch-stream' src='https://player.twitch.tv/?channel=" + response['hosted_channel'] + "&!autoplay' frameborder='0' scrolling='no'></iframe>");
                    $(".watch_button").html("<a href='https://www.twitch.tv/" + response['hosted_channel'] + "' target='_blank'><i class='fa fa-twitch' aria-hidden='true'></i> Auf Twitch gucken</a>");
                    onl_status = "hosting";
                } else {
                    onl_status = "offline";
                    //Offline und hostet nicht
                    if (response["_total"] > 0) {
                        //Past Broadcast vorhanden
                        $(".h3_stream_status").html("Stream (<span class='stream-offline'>Offline</span>)");
                        $(".online_stream_title").html("");
                        $(".online_stream_stat").html("<b>ThamianPlay</b> ist offline, aber hier siehst du seinen letzten Stream:");
                        $(".online_stream_title").html("<b>Status: </b>" + truncText(response['past_broadcast']['title'], 66));
                        $(".online_stream_title").attr("title", response['past_broadcast']['title']);
                        $(".online_stream_title").attr("uib-tooltip", response['past_broadcast']['title']);
                        $(".online_stream_title").attr("tooltip-trigger", "focus");
                        $(".online_stream_embed").html("<iframe class='embed-responsive-item twitch-stream' src='https://player.twitch.tv/?video=" + response['past_broadcast']['_id'] + "&!autoplay' frameborder='0' scrolling='no'></iframe>");
                        $(".watch_button").html("<a href='" + response['past_broadcast']['url'] + "' target='_blank'><i class='fa fa-twitch' aria-hidden='true'></i> Auf Twitch gucken</a>");
                    } else {
                        //Kein Past Broadcast vorhanden
                        $(".h3_stream_status").html("Stream (<span class='stream-offline'>Offline</span>)");
                        $(".online_stream_title").html("");
                        $(".online_stream_stat").html("<b>ThamianPlay</b> ist offline.");
                        $(".online_stream_embed").html("<img class='embed-responsive-item twitch-stream' src='" + response['offline_img'] + "' alt=''>")
                    }
                }
            }
        });

        $scope.getData = function () {
            $http.get("/stream/json").success(function (response) {
                if (response["is_offline"] == false) {
                    //Stream online
                    if (onl_status != "online") {
                        $(".h3_stream_status").html("Stream (<span class='stream-online'><i class='fa fa-video-camera' aria-hidden='true'></i> Online</span>)");
                        $(".online_stream_stat").html("<b>" + response['channel']['display_name'] + "</b> ist online und spielt <b>" + response['game'] + "</b> für <b>" + response['viewers'] + "</b> Zuschauer.");
                        $(".online_stream_title").html("<b>Status: </b>" + truncText(response['channel']['status'], 66));
                        $(".online_stream_title").attr("title", response['channel']['status']);
                        $(".online_stream_title").attr("tooltip", response['channel']['status']);
                        $(".online_stream_embed").html("<iframe class='embed-responsive-item twitch-stream' data-status='online' src='https://player.twitch.tv/?channel=" + response['channel']['name'] + "&!autoplay' frameborder='0' scrolling='no'></iframe>");
                        $(".watch_button").html("<a href='https://www.twitch.tv/" + response['channel']['name'] + "' target='_blank'><i class='fa fa-twitch' aria-hidden='true'></i> Auf Twitch gucken</a>");
                        onl_status = "online";
                    }
                } else {
                    //Stream offline
                    if (response["is_hosting"] == true) {
                        //Offline, aber hostet
                        if (onl_status != "hosting") {
                            $(".h3_stream_status").html("Stream (<span class='stream-offline'>Offline</span>)");
                            $(".online_stream_title").html("");
                            $(".online_stream_stat").html("<b>ThamianPlay</b> ist offline und hostet <b><a href='https://www.twitch.tv/" + response['hosted_channel'] + "' target='_blank'>" + response['hosted_channel'] + "</a></b>:");
                            $(".online_stream_embed").html("<iframe class='embed-responsive-item twitch-stream' src='https://player.twitch.tv/?channel=" + response['hosted_channel'] + "&!autoplay' frameborder='0' scrolling='no'></iframe>");
                            $(".watch_button").html("<a href='https://www.twitch.tv/" + response['hosted_channel'] + "' target='_blank'><i class='fa fa-twitch' aria-hidden='true'></i> Auf Twitch gucken</a>");
                            onl_status = "hosting";
                        }
                    } else {
                        //Offline und hostet nicht
                        if (onl_status != "offline") {
                            if (response["_total"] > 0) {
                                //Past Broadcast vorhanden
                                $(".h3_stream_status").html("Stream (<span class='stream-offline'>Offline</span>)");
                                $(".online_stream_title").html("");
                                $(".online_stream_stat").html("<b>ThamianPlay</b> ist offline, aber hier siehst du seinen letzten Stream:");
                                $(".online_stream_title").html("<b>Status: </b>" + truncText(response['past_broadcast']['title'], 66));
                                $(".online_stream_title").attr("title", response['past_broadcast']['title']);
                                $(".online_stream_title").attr("uib-tooltip", response['past_broadcast']['title']);
                                $(".online_stream_title").attr("tooltip-trigger", "focus");
                                $(".online_stream_embed").html("<iframe class='embed-responsive-item twitch-stream' src='https://player.twitch.tv/?video=" + response['past_broadcast']['_id'] + "&!autoplay' frameborder='0' scrolling='no'></iframe>");
                                $(".watch_button").html("<a href='" + response['past_broadcast']['url'] + "' target='_blank'><i class='fa fa-twitch' aria-hidden='true'></i> Auf Twitch gucken</a>");
                            } else {
                                //Kein Past Broadcast vorhanden
                                $(".h3_stream_status").html("Stream (<span class='stream-offline'>Offline</span>)");
                                $(".online_stream_title").html("");
                                $(".online_stream_stat").html("<b>ThamianPlay</b> ist offline.");
                                $(".online_stream_embed").html("<img class='embed-responsive-item twitch-stream' src='" + response['offline_img'] + "' alt=''>")
                            }
                            onl_status = "offline";
                        }
                    }
                }
            });
        }

        setInterval($scope.getData, 10000);
    });
});