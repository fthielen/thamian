$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

$(".social-facebook").on("mouseover", function () {
    $(this).find("i").css("color", "#3a5795");
    $(this).find("i").css("transition", "all 700ms");
});

$(".social-facebook").on("mouseout", function () {
    $(this).find("i").css("color", "#fff");
    $(this).find("i").css("transition", "all 700ms");
});

$(".social-twitter").on("mouseover", function () {
    $(this).find("i").css("color", "#0084B4");
    $(this).find("i").css("transition", "all 700ms");
});

$(".social-twitter").on("mouseout", function () {
    $(this).find("i").css("color", "#fff");
    $(this).find("i").css("transition", "all 700ms");
});

$(".social-instagram").on("mouseover", function () {
    $(this).find("i").css("color", "#3f729b");
    $(this).find("i").css("transition", "all 700ms");
});

$(".social-instagram").on("mouseout", function () {
    $(this).find("i").css("color", "#fff");
    $(this).find("i").css("transition", "all 700ms");
});

$(".social-twitch").on("mouseover", function () {
    $(this).find("i").css("color", "#6441a5");
    $(this).find("i").css("transition", "all 700ms");
});

$(".social-twitch").on("mouseout", function () {
    $(this).find("i").css("color", "#fff");
    $(this).find("i").css("transition", "all 700ms");
});

function genpw() {
    $.get("https://www.thamian.de/settings/user/genpw", function (data, status ){
        $('#password').get(0).type = 'text';
        $('#password_rp').get(0).type = 'text';
        $("#password, #password_rp").val(data);
    });
}

function resizemdcol() {
    var height = $(window).height() - ($(".navbar").height() + $("h1").height() + 60);
    $(".col-stream").css("height", height);
    $(".col-news").css("height", height);
    $(".news").css("height", height);
}

function resizeStream() {
    var height = $(".col-stream").height() / 4;
    var width = $(".col-stream").width() - 50;
    $(".twitch-stream").css("width", width);
    $(".twitch-stream").css("height", height);
}

function resizeNews() {
    $(".news").css("max-height", $(".col-news").height() - ($(".news").closest("h3").height() + 60));
}

function requestTeamspeakViewer() {
    $.get("https://www.thamian.de/server/teamspeak/viewer", function (data) {
        $(".modal_body_viewer").html(data);
    });
}

$("#modal_viewer").on("show.bs.modal", function () {
    requestTeamspeakViewer();
});

$("#modal_viewer").on("shown.bs.modal", function () {
    intv = setInterval(requestTeamspeakViewer, 10000);
});

$("#modal_viewer").on("hide.bs.modal", function () {
   clearInterval(intv);
});

//Block Perma
var ban_checkbox = $(".block_checkbox");
var ban_date = $(".block_date");

function checkPerma() {
    if (ban_checkbox.is(':checked')) {
        ban_date.prop('disabled', true);
    } else {
        ban_date.prop('disabled', false);
    }
}

function filterTable() {
    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("user_filter");
    filter = input.value.toUpperCase();
    table = document.getElementById("user_filter_table");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

$(ban_checkbox).on("click", function () {
    checkPerma();
});

$(document).ready(function () {
    $(".open_chug").on("click", function () {
        $("#chug_username").val($(this).data("uname"));
        $("#chug_uID").val($(this).data("uid"));
        $("#modal_changeUserGroup").modal("show");
    });

    $(".open_block_modal").on("click", function () {
        $(".contact_block_form_field").val($(this).data("email"));
        $("#modal_contact_block").modal("show");
    });

    $(".block_date").datepicker({
        dateFormat: 'dd.mm.yy',
        minDate: 0
    });

    resizemdcol();
    resizeNews();
    checkPerma();
});

$(window).on("resize", function () {
    resizemdcol();
    resizeNews();
});

$("#modal_useradd").on('hidden.bs.modal', function () {
    $('#password').get(0).type = 'password';
    $('#password_rp').get(0).type = 'password';
    $("#password, #password_rp").val("");
});

$(".h_form_btn").on("click", function () {
    var user = $(this).parent(".form_user_del").data("user");
    swal({
        title: 'Benutzer löschen?',
        text: "Bist du sicher, dass du den Benutzer <b>" + user + "</b> löschen willst?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d433',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Ja, Benutzer löschen!',
        cancelButtonText: 'Abbrechen'
    }).then(function(isConfirm) {
        if (isConfirm) {
            $(".form_user_del").submit();
        }
    })
});