/**
 * Created by fthielen on 06.09.2016.
 */
var thamian = angular.module('thamianNews', ["ngSanitize"]);

thamian.controller('NewsController', function ($scope, $http, $sce) {
    $http.get("/news/json").success(function (response) {
        $scope.news = response;
    });

    $http.get("/news/twitch/json").success(function (response) {
        $scope.twnews = response;
    });

    $scope.getData = function () {
        $http.get("/news/json").success(function (response) {
            $scope.news = response;
        });

        $http.get("/news/twitch/json").success(function (response) {
            $scope.twnews = response;
        });
    };

    setInterval($scope.getData, 20000);
});