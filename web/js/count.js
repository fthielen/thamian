var thamian = angular.module('thamian', []);

thamian.controller('CountController', function ($scope, $http) {
    $http.get("/backend/count/all").success(function (response) {
       $scope.count = response;
    });

    $http.get("/backend/count/gbuch").success(function (response) {
        $scope.countGB = response;
    });

    $http.get("/backend/count/sup").success(function (response) {
       $scope.countSup = response;
    });

    $scope.getData = function () {
        $http.get("/backend/count/all").success(function (response) {
            $.get("/sess_check", function (data) {
                if (data == "true") {
                    $scope.count = response;
                }
            });
        });

        $http.get("/backend/count/gbuch").success(function (response) {
            $.get("/sess_check", function (data) {
                if (data == "true") {
                    $scope.countGB = response;
                }
            });
        });

        $http.get("/backend/count/sup").success(function (response) {
            $.get("/sess_check", function (data) {
                if (data == "true") {
                    $scope.countSup = response;
                }
            });
        });
    };

    setInterval($scope.getData, 60000);
});