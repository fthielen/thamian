<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig_Extensions_' => array($vendorDir . '/twig/extensions/lib'),
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'Thamian' => array($baseDir . '/src'),
    'React\\ZMQ' => array($vendorDir . '/react/zmq/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'Pimple' => array($vendorDir . '/pimple/pimple/lib'),
    'Guzzle\\Stream' => array($vendorDir . '/guzzle/stream'),
    'Guzzle\\Parser' => array($vendorDir . '/guzzle/parser'),
    'Guzzle\\Http' => array($vendorDir . '/guzzle/http'),
    'Guzzle\\Common' => array($vendorDir . '/guzzle/common'),
    'Evenement' => array($vendorDir . '/evenement/evenement/src'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib'),
);
