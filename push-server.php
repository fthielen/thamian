<?php
/**
 * Created by IntelliJ IDEA.
 * User: fthielen
 * Date: 06.09.2016
 * Time: 16:58
 */

require_once __DIR__.'/vendor/autoload.php';

$loop = \React\EventLoop\Factory::create();
$pusher = new \Thamian\Pusher();

$context = new \React\ZMQ\Context($loop);
$pull = $context->getSocket(ZMQ::SOCKET_PULL);
$pull->bind('tcp://127.0.0.1:5555');
$pull->on('message', array($pusher, 'onNewsEntry'));

$webSock = new \React\Socket\Server($loop);
$webSock->listen(3000, '127.0.0.1');
$webServer = new \Ratchet\Server\IoServer(
    new \Ratchet\Http\HttpServer(
        new \Ratchet\WebSocket\WsServer(
            new \Ratchet\Wamp\WampServer(
                $pusher
            )
        )
    ),
    $webSock
);

$loop->run();