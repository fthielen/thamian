# Projekt: Thamian
## Beschreibung:
> Das Projekt "Thamian" ist eine Website für den **Twitch-Streamer** [Thamian](https://www.twitch.tv/thamianplay "Thamian").

Die Seite bietet folgende Funktionen:
### Frontend:
* Startseite, auf der die News (aus dem Backend und von der Twitch-API), Tweets und der Twitch-Stream angezeigt wird
* Über mich Seite
* Quotes und Befehle aus dem ChatBot
* Gästebuch
* Informationen über den Gamingserver
* Informationen über den Teamspeak-Server sowie die aktuell verbundenen Clients
* Kontaktformular

### Backend:
* Benutzerverwaltung (Benutzer einsehen, hinzufügen, löschen und deren Gruppe ändern)
* Den Inhalt der "Über mich" Seite ändern
* News hinzufügen/löschen
* Rechtevergabe für den Teamspeak-Server
* Passwort ändern
* Supportanfragen einsehen, beantworten und löschen
* Gästebucheinträge einsehen, freischalten und löschen
* Nutzer der Supportanfragen und Gästebucheinträge lassen sich blockieren, wenn unangemessene bzw. verbotene Inhalte gesendet werden. Diese können dann nicht mehr das Kontaktformular nutzen, und Gästebucheinträge hinzufügen
* Blockierte Benutzer einsehen und wieder freigeben

***
## Technische Informationen:
Als Grundlage für das Projekt dient das **PHP-Framework** [Silex](http://silex.sensiolabs.org/ "Silex").  
Dieses basiert auf dem mächtigen [Symfony-Framework](https://symfony.com/ "Symfony").  

### Anforderungen an den Server:
 * PHP >= 5.5.9
 * JSON muss aktiviert sein
 * ctype muss aktiviert sein
 * Für Doctrine muss PDO aktiviert sein
 * Die korrekte Zeitzone muss in der php.ini gesetzt sein  

Folgende **Composer-Pakete** werden genutzt:
```json
{
  "silex/silex": "^1.3",
  "twig/twig": "^1.24",
  "symfony/form": "^2.3",
  "symfony/validator": "^2.3",
  "symfony/config": "^2.3",
  "symfony/translation": "^2.3",
  "symfony/security-csrf": "^2.3",
  "symfony/twig-bridge": "^2.3",
  "twig/extensions": "^1.3",
  "doctrine/dbal": "~2.2",
  "guzzlehttp/guzzle": "^6.2",
  "phpmailer/phpmailer": "^5.2",
  "cboden/ratchet": "^0.3.4",
  "react/zmq": "^0.3.0"
}
```

***
Zusätzlich werden noch folgende Bibliotheken genutzt:
* [Teamspeak 3 PHP Framework](https://docs.planetteamspeak.com/ts3/php/framework/index.html)
* [PhantomBot PHP Connector](https://github.com/PhantomBot/PhantomBot-PHP-Connector)  

Im Frontend werden folgende Bibliotheken genutzt:
* [Bootstrap CSS Framework](http://getbootstrap.com/)
* [jQuery](https://jquery.com/)
* [SweetAlert 2](https://limonte.github.io/sweetalert2/)
* [AngularJS](https://angularjs.org/)
* [Angular Sanitize](https://docs.angularjs.org/api/ngSanitize/service/$sanitize)
* [jQuery Custom Scrollbar](http://manos.malihu.gr/jquery-custom-content-scroller/)
* [Font Awesome](http://fontawesome.io/)
* [TinyMCE Editor](https://www.tinymce.com/)  

***
Um die Stylesheets zu erweitern, und das Scripten zu erleichtern, verwende ich den **CSS Präprozessor** [Sass](http://sass-lang.com/).  
So sind **Sass-Stylesheets** grundsätzlich aufgebaut:
```sass
.table-custom
  border: 4px solid #000
  margin-bottom: 20px
  th
    border: 3px solid #000 !important
  td
    border: 2px solid #000 !important
  th, td
    padding: 5px 10px
    text-align: center
```

Das Stylesheet in CSS kompiliert:
```css
.table-custom {
  border: 4px solid #000;
  margin-bottom: 20px; }
  .table-custom th {
    border: 3px solid #000 !important; }
  .table-custom td {
    border: 2px solid #000 !important; }
  .table-custom th, .table-custom td {
    padding: 5px 10px;
    text-align: center; }
```

***
### Sicherheit:
Das Framework ist von Haus aus schon gegen **XSS (Cross Site Scripting)** und **MySQL-Injection** abgesichert.  

Jedoch musste ich einen Schutz gegen **CSRF (Cross Site Request Forgery)** implementieren.  
CSRF ist grob gesagt das **manipulieren von Anfragen**.  
Um sich dagegen zu schützen, wird jedem Formular zusätzlich ein **Token** hinzugefügt, der bei jedem Aufruf der Seite neu generiert wird.  
Dieser wird auch in eine Session gespeichert.  
Wenn die Anfrage aus dem Formular verarbeitet wird, wird der Token aus dem Formular mit dem Token aus der Session abgeglichen.  
Dadurch wird sicher gestellt, dass Anfragen nur von der Webanwendung gestellt werden können. 
### Beispiel-Code: 
#### Code im Frontend:
```php
<?php
  session_start();

  //Session leeren
  unset($_SESSION["_token"]);

  //Neuen Token generieren
  $_SESSION["_token"] = bin2hex(random_bytes(32));
?>
```
  
```html
<!DOCTYPE html>
<html>
<head>
  <title>CSRF</title>
</head>
<body>
  <form method="post" action="login.php">
    <input type="text" name="username">
    <input type="password" name="password">
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['_token']; ?>">

    <input type="submit" value="Login">
  </form>
</body>
</html>
```

***
#### Code im Backend:
```php
<?php
  session_start();

  $username = $_POST["username"];
  $password = $_POST["password"];

  $post_token = $_POST["csrf_token"];
  $sess_token = $_SESSION["_token"];

  //Existiert der Token, und stimmt dieser überein?
  if (isset($post_token, $sess_token) && $post_token == $sess_token) {
    //Ja --> Mit dem Login fortfahren
  } else {
    //Nein --> Gefälschte Anfrage
    echo "CSRF Token mismatch!";
  }
?>
```

***
### In meinem Projekt:
#### Code im Frontend:
```html
<form class="form-horizontal" action="{{ path('devlogin_post') }}" method="post">
    <fieldset>
        <div class="form-group">
            <label class="col-md-4 control-label" for="username">Benutzername</label>
            <div class="col-md-4">
                <input id="username" name="username" type="text" placeholder="Benutzername" class="form-control input-md" required="">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Passwort</label>
            <div class="col-md-4">
                <input id="password" name="password" type="password" placeholder="Passwort" class="form-control input-md" required="">
            </div>
        </div>

        <!-- CSRF Token -->
        <input type="hidden" name="csrf_token" value="{{ csrf_token }}">

        <div class="form-group">
            <label class="col-md-4 control-label" for=""></label>
            <div class="col-md-4">
                <button id="" name="" class="btn btn-primary">Anmelden</button>
            </div>
        </div>

    </fieldset>
</form>
```

***
#### Code im Backend:
```php
<?php
  $app->post('/login', function (\Symfony\Component\HttpFoundation\Request $request) use ($app) {
      $username = $request->get('username');
      $password = hash('whirlpool', $request->get('password') . $username . $app['secret_salt']);

      $ptoken = $request->get('csrf_token');
      $stoken = $app['session']->get('_token');

      //Existiert der Token, und stimmt dieser überein?
      if (isset($ptoken, $stoken) && $ptoken == $stoken) {
          //Ja --> Fortfahren
          $sql = "SELECT u.`ID`, u.`Username`, u.`Password`, g.`Name` FROM `User` u JOIN `Groups` g ON u.gID = g.ID WHERE Username = ?";
          $get_user = $app['db']->fetchAssoc($sql, array((string) $username));

          if ($get_user['Password'] == $password) {
              $app['session']->set('Username', $get_user['Username']);
              $app['session']->set('Group', $get_user['Name']);
              $app['session']->set('uID', $get_user['ID']);

              $update = "UPDATE `User` SET `Last_Login` = ? WHERE `Username` = ?";
              $app['db']->executeUpdate($update, array((string) date("d.m.Y, H:i:s"), (string) $username));

              $app['session']->getFlashBag()->add('Success', 'Erfolgreich angemeldet!');

              return $app->redirect('/');
          } else {
              $app['session']->getFlashBag()->add('Error', 'Benutzername und/oder Passwort falsch!');
              return $app->redirect($app['url_generator']->generate('devlogin'));
          }
      } else {
          //Nein == gefälschte Anfrage --> Fehler in der Session speichern und zurück leiten
          $app['session']->getFlashBag()->add('Error', 'CSRF Token mismatch!');
          return $app->redirect($app['url_generator']->generate('devlogin'));
      }
  })
      ->bind('devlogin_post')
  ;
?>
```
***
## Validierung:
Um sicher zu stellen, dass in Formulare auch nur richtige Daten, bzw. Passwörter mit richtiger Länge angegeben werden, kommt Validation zum Einsatz.  
Dies realisiere ich mit dem Paket **symfony/validator**.  
Ein Beispiel an Hand der **"Passwort ändern"** Funktion:
```php
<?php
  $validate = array(
      'password' => $request->get('npw')
  );

  $constraint = new \Symfony\Component\Validator\Constraints\Collection(array(
      'password' => new \Symfony\Component\Validator\Constraints\Length(array(
          'min' => '10'
      ))
  ));

  $errors = $app['validator']->validateValue($validate, $constraint);

  //Entspricht das Passwort den Mindestanforderungen?
  if (count($errors) > 0) {
      /**
       * Nein --> Fehler ausgeben und zurück leiten
       */
      $app['session']->getFlashBag()->add('Error', 'Das Passwort muss mindestens 10 Zeichen lang sein!');
      return $app->redirect($app['url_generator']->generate('settings.user.password'));
  } else {
      /**
       * Ja --> Fortfahren
       */
      $app['db']->update('User', array(
          'Password' => $pwnew
      ), array(
          'Password' => $pwold
      ));

      $app['session']->getFlashBag()->add('Success', 'Du hast dein Passwort erfolgreich geändert!');
      return $app->redirect($app['url_generator']->generate('settings.user.password'));
  }
?>
```

***
##### ERD:
![ERD][ERD]
[ERD]: https://screenshots.clocxhd.de/ERD_Thamian.png "ERD"
***